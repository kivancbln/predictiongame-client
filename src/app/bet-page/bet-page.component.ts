import { Component, OnInit, Input, AfterViewInit, ChangeDetectorRef, HostListener, Output, EventEmitter } from '@angular/core';
import { NavParams, ModalController, AlertController, ToastController, PickerController } from '@ionic/angular';
import { RestService } from '../services/rest.service';
import { ActiveAd, SharedService } from '../services/shared.service';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { AdMobFree, AdMobFreeRewardVideoConfig } from '@ionic-native/admob-free/ngx';

@Component({
  selector: 'app-bet-page',
  templateUrl: './bet-page.component.html',
  styleUrls: ['./bet-page.component.scss'],
})
export class BetPageComponent implements OnInit, AfterViewInit {
  @Input() match: any;
  @Input() modalCtrl: ModalController;
  @Input() points: any;
  @Input() disabled = false;
  @Input() entered = true;
  @Input() challengeId: any;
  @Input() challengeMaxPoint = -1;
  @Input() challengeAllPoints = -1;

  betAway: number;
  betHome: number;
  betPoints: number;
  bets: any;
  oddDescs: any;
  oddId: any;

  constructor(private readonly rest: RestService, readonly navParams: NavParams,
              public alertController: AlertController,
              private readonly ref: ChangeDetectorRef,
              private readonly toastController: ToastController,
              private readonly shared: SharedService,
              private readonly globalization: Globalization,
              private readonly translate: TranslateService,
              private readonly pickerController: PickerController,
              private readonly sharedService: SharedService,
              private readonly admobFree: AdMobFree
  ) {
    this.modalCtrl = navParams.get('modalCtrl');
    this.match = navParams.get('match');
    const oddDescObj = this.match.MatchOdds.reduce((ubc, u) => ({
      ...ubc,
      [u.oddDesc]: [...(ubc[u.oddDesc] || []), u],
    }), {});
    delete oddDescObj['Match Winner'];
    this.oddDescs = Object.entries(oddDescObj).map(([key, value]) => ({ key, value }));
    this.oddDescs.forEach(oddDesc => {
      oddDesc.expanded = false;
    });
  }

  async ngAfterViewInit(): Promise<void> {

    // await this.getParameters(this.reportid);
    // this.bets = await this.rest.getPreviousBetsByUserByMatch(await this.rest.getUserId(), this.match['ID']);

    // this.chart.instance.option("size",{height:this.height,width:this.width})
  }
  ngOnInit() {
    this.bets = this.match.Bets;
    const usr: any = this.rest.loggedInUser;
    const userid = usr === undefined ? -1 : usr.account.ID;
    // this.rest.createActionLog({ userid, action: 'Bet page entered' });
  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }

  dismiss(flag) {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      dismissed: flag
    });
  }

  async bet(factor = 1) {
    try {
      this.alertController.create();
      this.shared.presentLoading();
      const now = new Date();
      const matchdatenumeric = new Date(this.match.date);
      if (now > matchdatenumeric) {
        this.shared.dismissLoading();
        await this.presentToast(this.translate.instant('Match is already started'));
        return;
      }
      const bet: any = {};
      bet.userId = await this.rest.getUserId();
      bet.matchId = this.match.ID;
      bet.points = parseInt(this.betPoints.toString(), 10);
      bet.challengeId = this.challengeId;
      bet.oddId = this.oddId;
      bet.factor = factor;
      this.points = await this.sharedService.getPoints();
      if (this.points - bet.points < 0) {
        this.shared.dismissLoading();
        this.presentToast(this.translate.instant('Insufficient points'));
      } else {
        await this.rest.createBetByUserByMatch(bet);
        this.presentToast(this.translate.instant('Prediction is saved'));
        await this.sharedService.getPoints();
        this.match = (await this.rest.getMatchById(this.match.ID))[0];
        this.bets = this.match.Bets;
        this.shared.dismissLoading();
      }
    } catch (error) {
      console.log('error', error);
    } finally {
      this.shared.dismissLoading();

    }
  }

  setBetHome(val) {
    this.betHome = val;
  }

  setBetAway(val) {
    this.betAway = val;
  }

  setBetPoints(val) {
    this.betPoints = val;
  }

  openBetPage(i) {

  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Denied',
      message,
      buttons: ['OK']
    });
    await alert.present();
  }

  async deleteBet(betid) {

    this.shared.presentLoading();
    const now = new Date();
    const matchdatenumeric = new Date(this.match.date);
    if (now > matchdatenumeric) {
      this.shared.dismissLoading();
      await this.presentAlert('Too late..');
    } else {
      await this.rest.deleteBet(betid);

      // this.bets = await this.rest.getPreviousBetsByUserByMatch(await this.rest.getUserId(), this.match['ID']);
      const deleted = this.bets.filter(x => x.ID === betid)[0];
      const ix = this.bets.indexOf(deleted);
      if (ix > -1) {
        this.bets.splice(ix, 1);
      }
      this.shared.dismissLoading();
      this.ref.detectChanges();
      this.presentToast(this.translate.instant('Prediction is deleted'));
    }
  }



  async presentToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  getLocaleString(date) {
    return (new Date(date)).toLocaleString();
  }

  getColumns(numColumns, numOptions, columnOptions) {
    const columns = [];
    for (let i = 0; i < numColumns; i++) {
      columns.push({
        name: `col-${i}`,
        options: this.getColumnOptions(i, numOptions, columnOptions)
      });
    }

    return columns;
  }

  getColumnOptions(columnIndex, numOptions, columnOptions) {
    const options = [];
    for (let i = 0; i < numOptions; i++) {
      options.push({
        text: columnOptions[columnIndex][i % numOptions],
        value: i
      });
    }

    return options;
  }

  getEnterANumber() {
    this.translate.instant('enter a number');
  }

  expandCollapse(e, oddDesc) {
    oddDesc.expanded = !(oddDesc.expanded || null || false);
    this.ref.detectChanges();
  }

  async openPicker(oddid) {
    const now = new Date();
    const matchdatenumeric = new Date(this.match.date);
    if (now > matchdatenumeric) {
      await this.presentToast(this.translate.instant('Match is already started'));
      return;
    }

    if (!this.entered) {
      await this.presentToast('You need to enter challenge first');
      return;
    }
    const columns = [];


    const pointsOptions = [];
    for (let i = 1; i <= 10; i++) {
      pointsOptions.push({
        text: i * 5,
        value: i * 5
      });
    }

    columns.push({
      name: `col-1`,
      options: pointsOptions
    });

    const picker = await this.pickerController.create({
      columns,
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          role: 'cancel'
        },
        {
          text: this.translate.instant('Confirm'),
          handler: (value) => {
            this.oddId = oddid;
            this.betPoints = value['col-1'].value;
            this.presentConfirm();
          }
        }
      ]
    });

    await picker.present();
  }

  async presentConfirm() {

    if (this.challengeMaxPoint > -1 && this.challengeAllPoints > -1) {
      if (this.challengeAllPoints + this.betPoints > this.challengeMaxPoint) {
        await this.presentToast(this.translate.instant('Spent: ')
          + this.challengeAllPoints + '\n' + this.translate.instant('Allowed: ' + this.challengeMaxPoint));
        return;
      }
    }


    const alert = await this.alertController.create({
      message: this.translate.instant('Would like to double your earning by watching video?'),
      buttons: [
        {
          text: this.translate.instant('No'),
          role: 'cancel',
          handler: async () => {
            await this.bet();
          }
        },
        {
          text: this.translate.instant('Yes'),
          handler: async () => {
            await this.openRewardedVideo();
          }
        }
      ]
    });
    await alert.present();
  }

  async openRewardedVideo() {
    try {
      ActiveAd.adid = this.match.ID;
      const RewardVideoConfig: AdMobFreeRewardVideoConfig = {
        // isTesting: true, // Remove in production
        autoShow: true, // ,
        id: 'ca-app-pub-3709541018243663/5816535635'
      };
      this.admobFree.rewardVideo.config(RewardVideoConfig);
      this.sharedService.presentLoading();
      const res = await this.admobFree.rewardVideo.prepare();

    } catch (e) {
      alert(e);
    }
  }

  sortBy(obj,prop: string) {
    return obj.sort((a, b) => a[prop] > b[prop] ? 1 : a[prop] === b[prop] ? 0 : -1);
  }

  @HostListener('document:admob.rewardvideo.events.REWARD', ['$event'])
  async rewardEnter(event: any) {
    console.log('reward catched');
    console.log(ActiveAd.adid);
    console.log(event);
    if (ActiveAd.adid === this.match.ID && event.returnValue) {
      console.log('entered bet 2');
      await this.bet(2);
      ActiveAd.adid = '';
    }
  }

  @HostListener('document:admob.rewardvideo.events.CLOSE', ['$event'])
  async closeEnter(event: any) {

    if (ActiveAd.adid === this.match.ID && !event.returnValue) {
      alert('video closed');
      this.sharedService.dismissLoading();
      await this.bet();
      ActiveAd.adid = '';
    }
  }

  @HostListener('document:admob.rewardvideo.events.LOAD_FAIL', ['$event'])
  async loadFailEnter(event: any) {
    if (ActiveAd.adid === this.match.ID) {
      alert('video load failed');
      this.sharedService.dismissLoading();
      await this.bet();
      ActiveAd.adid = '';
    }
  }

  @HostListener('document:admob.rewardvideo.events.EXIT_APP', ['$event'])
  async exitAppEnter(event: any) {
    this.sharedService.dismissLoading();
  }
}
