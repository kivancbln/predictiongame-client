import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { RestService } from '../services/rest.service';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../services/shared.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss'],
})
export class RankingComponent implements OnInit {
  @Input() year: number;
  @Input() month: number;
  rankings: any;

  constructor(private rest: RestService,
              private ref: ChangeDetectorRef,
              private globalization: Globalization,
              private translate: TranslateService,
              private sharedService: SharedService,
              private modalController: ModalController) { }

  async ngOnInit(): Promise<void> {
    this.getRankings().then(() => {
      this.ref.detectChanges();
    });
  }

  async getRankings() {
    this.sharedService.presentLoading();
    this.rankings = await this.rest.getRankings(this.year, this.month);
    this.ref.detectChanges();
    this.sharedService.dismissLoading();
  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }

  dismiss() {
    this.modalController.dismiss()
  }

}
