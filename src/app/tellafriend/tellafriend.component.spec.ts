import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TellafriendComponent } from './tellafriend.component';

describe('TellafriendComponent', () => {
  let component: TellafriendComponent;
  let fixture: ComponentFixture<TellafriendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TellafriendComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TellafriendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
