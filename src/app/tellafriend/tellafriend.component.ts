import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { AlertService } from '../services/alert.service';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { Globalization } from '@ionic-native/globalization/ngx';
import { RestService } from '../services/rest.service';

@Component({
  selector: 'app-tellafriend',
  templateUrl: './tellafriend.component.html',
  styleUrls: ['./tellafriend.component.scss'],
})
export class TellafriendComponent implements OnInit {

  link = 'https://play.google.com/store/apps/details?id=io.ionic.predictiongame2';
  id: number;
  constructor(
    private rest: RestService,
    private clipboard: Clipboard,
    private modalController: ModalController,
    private ref: ChangeDetectorRef,
    private translate: TranslateService,
    private globalization: Globalization,
    private alertService: AlertService) {  }

  ngOnInit() { 
    this.id = this.rest.loggedInUser['account'].ID;
  }
  async copyLink() {
    await this.clipboard.copy(this.link);
    this.alertService.presentToast(this.translate.instant('Link copied to clipboard'));
  }

  dismiss() {
    this.modalController.dismiss();
  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }

}
