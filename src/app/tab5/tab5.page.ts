import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { RestService } from '../services/rest.service';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page implements OnInit {
  buyables: any[] = [];

  constructor(
    private rest: RestService,
    private sharedService: SharedService,
    private ref: ChangeDetectorRef
  ) { }

  async ngOnInit(): Promise<void> {
    this.buyables = await this.rest.getBuyables();
    this.ref.detectChanges();
  }

  

}
