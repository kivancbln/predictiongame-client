import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VERSION } from '../../environments/version';
import { Platform, MenuController, ModalController, AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';
import { SharedService } from '../services/shared.service';
import { RankingComponent } from '../ranking/ranking.component';
import { RestService } from '../services/rest.service';
import { ProfileComponent } from '../profile/profile.component';
import { ActivePredictionsComponent } from '../active-predictions/active-predictions.component';
import { NotificationComponent } from '../notification/notification.component';
import { ReferenceComponent } from '../reference/reference.component';
import { TellafriendComponent } from '../tellafriend/tellafriend.component';


@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {
  version = VERSION;
  notifications: any = [];
  constructor(private router: Router,
    private platform: Platform,
    private ref: ChangeDetectorRef,
    private globalization: Globalization,
    private translate: TranslateService,
    private menuController: MenuController,
    private sharedService: SharedService,
    private modalController: ModalController,
    private alertController: AlertController,
    private rest: RestService) { }
  async ngOnInit(): Promise<void> {
    this.rest.getNotifications().then(data => {
      this.notifications = data;
    });

    console.log('user', this.rest.loggedInUser);
  }

  gotoTab1(e) {
    // this.router.navigateByUrl('/dashboard/tabs/tab1');
  }

  gotoTab2(e) {
    // this.router.navigateByUrl('/dashboard/tabs/tab2');
  }

  isMobile() {
    return this.platform.is('android') || this.platform.is('ios');
  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }

  openMenu() {
    this.menuController.open();
  }

  async openRankingPage() {
    this.sharedService.presentLoading();

    const modal = await this.modalController.create({
      component: RankingComponent,
      componentProps: {
        year: (new Date()).getFullYear().toString(),
        month: ((new Date()).getMonth() + 1).toString()
      }
    });
    this.sharedService.dismissLoading();
    return await modal.present();
  }

  async openProfilePage() {
    this.sharedService.presentLoading();

    const modal = await this.modalController.create({
      component: ProfileComponent
    });
    this.sharedService.dismissLoading();
    return await modal.present();
  }

  async openActivePredictions() {
    this.sharedService.presentLoading();

    const modal = await this.modalController.create({
      component: ActivePredictionsComponent,
      componentProps: {
        isactive: true
      }
    });
    this.sharedService.dismissLoading();
    return await modal.present();
  }

  async openFinishedPredictions() {
    this.sharedService.presentLoading();

    const modal = await this.modalController.create({
      component: ActivePredictionsComponent,
      componentProps: {
        isactive: false
      }
    });


    this.sharedService.dismissLoading();
    return await modal.present();
  }

  async openNotifications() {
    this.sharedService.presentLoading();

    const modal = await this.modalController.create({
      component: NotificationComponent,
      componentProps: {
        notifications: this.notifications.filter(x => x.NotificationRead.ID === 0)
      }
    });
    modal.onDidDismiss().then(data => {
      this.rest.getNotifications().then(data2 => {
        this.notifications = data2;
      });

    });
    this.sharedService.dismissLoading();
    return await modal.present();

  }

  checkNotifications() {
    if (this.notifications) {
      return this.notifications.some(x => x.NotificationRead.ID === 0);
    } else {
      return false;
    }
  }

  isMarketActive() {
    const usr: any = this.rest.loggedInUser;
    if (usr !== undefined && usr.account !== undefined && usr.account.GeneralSetting !== undefined) {
      
      return usr.account.GeneralSetting.marketActive && this.version.version === usr.account.GeneralSetting.version
    } else {
      return false;
    }
  }

  async openReferencePage() {

    const usr: any = this.rest.loggedInUser;
    if (usr['account'].ReferenceUserID !== 0) {
      this.presentAlert(this.translate.instant('You already entered a reference user.'));
      return;
    }
    this.sharedService.presentLoading();

    const modal = await this.modalController.create({
      component: ReferenceComponent
    });
    modal.onDidDismiss().then(data => {

    });
    this.sharedService.dismissLoading();
    return await modal.present();
  }

  async openTellAFriend() {
    this.sharedService.presentLoading();

    const modal = await this.modalController.create({
      component: TellafriendComponent
    });
    modal.onDidDismiss().then(data => {

    });
    this.sharedService.dismissLoading();
    return await modal.present();
  }

  logout() {
    this.rest.logout();
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Denied',
      message,
      buttons: ['OK']
    });
    await alert.present();
  }


}
