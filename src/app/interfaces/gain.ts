export interface Gain {
    userId: number;
    matchId: number;
    type: number;
    challengeId: number;
    gainedPoints: number;
}
