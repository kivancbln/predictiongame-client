export interface LoginResponse {
    ID: number;
    email: string;
    token: string;
    uuid: string;
    serial: string;
    model: string;
    guest: boolean;
    points: number;
}
