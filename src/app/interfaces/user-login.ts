export interface UserLogin {
    email: string;
    password: string;
    uuid: string;
    serial: string;
    model: string;
    guest: boolean;
}
