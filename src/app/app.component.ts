import { Component, OnInit, OnDestroy } from '@angular/core';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SocketService } from './services/socket.service';

import { VERSION } from '../environments/version';
import { TranslateService } from '@ngx-translate/core';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { SharedService } from './services/shared.service';
import { RestService } from './services/rest.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  version = VERSION;
  messages: any[];
  chatBox: string;
  constructor(
    private readonly platform: Platform,
    private readonly splashScreen: SplashScreen,
    private readonly statusBar: StatusBar,
    private readonly socket: SocketService,
    private readonly translate: TranslateService,
    private readonly oneSignal: OneSignal,
    private readonly alertCtrl: AlertController,
    private readonly sharedService: SharedService,
    private readonly rest: RestService
  ) {
    this.initializeApp();
    this.messages = [];
    this.chatBox = '';
  }

  initializeApp() {
    if (this.platform.is('android')) {
      this.platform.ready().then(() => {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
      });
      this.setupPush();
    } else if (this.platform.is('ios')) {
      this.setupPushIOS();
    }
  }

  public ngOnInit() {
    this.socket.getEventListener().subscribe(event => {
      if (event.type === 'message') {
        let data = event.data.content;
        if (event.data.sender) {
          data = `${event.data.sender}: ${data}`;
        }
        this.messages.push(data);
        console.log(this.messages);
      }
      if (event.type === 'close') {
        this.messages.push('/The socket connection has been closed');
      }
      if (event.type === 'open') {
        this.messages.push('/The socket connection has been established');
      }
    });
  }

  public ngOnDestroy() {
    this.socket.close();
  }

  public send() {
    if (this.chatBox) {
      this.socket.send(this.chatBox);
      this.chatBox = '';
    }
  }

  public isSystemMessage(message: string) {
    return message.startsWith('/') ? `<strong>${message.substring(1)}</strong>` : message;
  }

  setupPush() {
    // I recommend to put these into your environment.ts
    this.oneSignal.startInit('ba4bc4e7-0547-49b9-8c16-f1f8d9b4225a', 'io.ionic.predictiongame2');
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);


    // Notifcation was received in general
    this.oneSignal.handleNotificationReceived().subscribe(data => {
      const msg = data.payload.body;
      const title = data.payload.title;
      const additionalData = data.payload.additionalData;
      this.showAlert(title, msg, additionalData.task);
    });

    // Notification was really clicked/opened
    this.oneSignal.handleNotificationOpened().subscribe(data => {
      // Just a note that the data is a different place here!
      const additionalData = data.notification.payload.additionalData;

      this.showAlert('Notification opened', 'You already read this before', additionalData.task);
    });

    this.oneSignal.endInit();

    this.sharedService.loggedin$.subscribe(ret => {
      console.log('subs', ret);
      if (ret) {
        this.oneSignal.getIds().then(oneSignalIdentity => {
          this.rest.updateSubscriptionId(oneSignalIdentity.userId);
        });
      }
    });

  }

  async showAlert(title, msg, task) {
    const alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: [
        {
          text: `Action: ${task}`,
          handler: () => {
            // E.g: Navigate to a specific screen
          }
        }
      ]
    });
    alert.present();
  }

  setupPushIOS() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      //Remove this method to stop OneSignal Debugging 
      window["plugins"].OneSignal.setLogLevel({ logLevel: 6, visualLevel: 0 });

      var notificationOpenedCallback = function (jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      };

      // Set your iOS Settings
      var iosSettings = {};
      iosSettings["kOSSettingsKeyAutoPrompt"] = false;
      iosSettings["kOSSettingsKeyInAppLaunchURL"] = false;

      window["plugins"].OneSignal
        .startInit('ba4bc4e7-0547-49b9-8c16-f1f8d9b4225a')
        .handleNotificationOpened(notificationOpenedCallback)
        .iOSSettings(iosSettings)
        .inFocusDisplaying(window["plugins"].OneSignal.OSInFocusDisplayOption.Notification)
        .endInit();

      // The promptForPushNotificationsWithUserResponse function will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 6)
      window["plugins"].OneSignal.promptForPushNotificationsWithUserResponse(function (accepted) {
        console.log("User accepted notifications: " + accepted);
      });
    });
  }
}
