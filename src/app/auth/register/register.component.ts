import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginComponent } from '../login/login.component';
import { RestService } from '../../services/rest.service';
import { ModalController, NavController } from '@ionic/angular';
import { AlertService } from '../../services/alert.service';
import { UserLogin } from '../../interfaces/user-login';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {

  constructor(private readonly modalController: ModalController,
              private readonly rest: RestService,
              private readonly navCtrl: NavController,
              private readonly alertService: AlertService,
              private readonly ref:ChangeDetectorRef,
              private readonly globalization:Globalization,
              private readonly translate:TranslateService
  ) { }
  ngOnInit() {
  }

  ionViewDidEnter(){
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if(lg.value === 'tr-TR'){
        this.translate.use(lg.value);
      }
    });
  }

  // Dismiss Register Modal
  dismissRegister() {
    this.modalController.dismiss();
  }
  // On Login button tap, dismiss Register modal and open login Modal
  async loginModal() {
    this.dismissRegister();
    const loginModal = await this.modalController.create({
      component: LoginComponent,
    });
    return await loginModal.present();
  }
  async register(form: NgForm) {
    const user = {} as UserLogin;
    user.email = form.value.email;
    user.password = form.value.password;
    const ret: any = await this.rest.register(user);
    this.alertService.presentToast(ret.message);

    if (ret.status === true) {
      
      await this.rest.login(user);
      await this.rest.earnPoints(100, 2);
      this.dismissRegister();
      this.navCtrl.navigateRoot('/dashboard/tabs/tab1');
    }
  }
}
