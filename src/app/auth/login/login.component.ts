import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { RegisterComponent } from '../register/register.component';
import { ModalController, NavController, Platform, LoadingController } from '@ionic/angular';
import { RestService } from '../../services/rest.service';
import { NgForm } from '@angular/forms';
import { UserLogin } from '../../interfaces/user-login';
import { AlertService } from '../../services/alert.service';
import { Router } from '@angular/router';
import { SocketService } from '../../services/socket.service';
import * as firebase from 'firebase';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { SharedService } from '../../services/shared.service';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { AdMobFreeBannerConfig, AdMobFree } from '@ionic-native/admob-free/ngx';
import { Device } from '@ionic-native/device/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  isLoading: boolean;
  tab1Str = '/dashboard/tabs/tab1';

  constructor(
    private readonly modalController: ModalController,
    private readonly rest: RestService,
    private readonly navCtrl: NavController,
    private readonly alertService: AlertService,
    private readonly router: Router,
    private readonly socket: SocketService,
    private readonly fb: Facebook,
    public afAuth: AngularFireAuth,
    private readonly platform: Platform,
    public loadingController: LoadingController,
    private readonly shared: SharedService,
    private readonly ref: ChangeDetectorRef,
    private readonly globalization: Globalization,
    private readonly translate: TranslateService,
    private readonly google: GooglePlus,
    private readonly admobFree: AdMobFree,
    private readonly device: Device
  ) { }
  async ngOnInit(): Promise<void> {

    const val: any = await this.shared.getStorage('fblogin');

    if (val === 1) {
      this.presentLoading();
    }
    this.afAuth.getRedirectResult().then(result => {
      if (result.credential) {
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        this.afterLogin(result);
        // ...
      }
    }).catch(error => {
      console.log(error);
      this.dismiss();
    });
    this.shared.removeKey('fblogin');
  }

  async ionViewDidEnter(): Promise<void> {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });

    const token = await this.rest.getAccessToken();
    if (token !== '' && token !== null && token !== undefined) {
      this.dismissLogin();
      this.router.navigateByUrl(this.tab1Str);

    }

    this.showBannerAd();
    await this.admobFree.banner.prepare();
    await this.admobFree.banner.show();
  }

  async ionViewWillLeave(): Promise<void> {
    await this.admobFree.banner.hide();
  }


  // Dismiss Login Modal
  dismissLogin() {

    this.shared.removeKey('fblogin');
    this.loadingController.dismiss();
    this.modalController.dismiss();
  }
  // On Register button tap, dismiss login modal and open register modal
  async registerModal() {
    this.dismissLogin();
    const registerModal = await this.modalController.create({
      component: RegisterComponent
    });
    return await registerModal.present();
  }
  async login(form: NgForm) {


    const user = {} as UserLogin;
    user.email = form.value.email;
    user.password = form.value.password;
    const loggedin: any = await this.rest.login(user);
    this.alertService.presentToast(loggedin.message);
    if (loggedin.status === true) {
      this.alertService.presentToast('Logged In');
      this.shared.loggedin.next(true);
      this.dismissLogin();
      this.router.navigateByUrl(this.tab1Str);
    }
  }

  async loginFacebook() {
    await this.shared.setStorage('fblogin', 1);

    if (this.platform.is('android') || this.platform.is('ios')) {
      this.loginWFacebookANDROID();
    } else {
      this.loginWFacebookWEB();
    }
  }

  async loginWFacebookANDROID() {
    this.presentLoading();
    this.fb.login(['email'])
      .then((response: FacebookLoginResponse) => {
        this.onLoginSuccess(response);
        console.log(response);
      }).catch((error) => {
        console.log(error);
        alert('error:' + error);
      });
  }

  async loginGoogle() {
    const params = {
      webClientId: '533856061812-sam63qmcjk8aqcqtsadkh9sbtvo6hs7q.apps.googleusercontent.com',
      offline: true,
      scopes: 'email'
    };
    this.google.login(params)
      .then((response) => {
        const { idToken, accessToken } = response;
        this.onGoogleLoginSuccess(idToken, accessToken);
      }).catch((error) => {
        console.log(error);
        alert('error:' + JSON.stringify(error));
      });
  }

  onGoogleLoginSuccess(accessToken, accessSecret) {
    const credential = accessSecret ? firebase.auth.GoogleAuthProvider
      .credential(accessToken, accessSecret) : firebase.auth.GoogleAuthProvider
        .credential(accessToken);
    this.afAuth.signInWithCredential(credential)
      .then((response) => {
        this.afterLogin(response);
      });

  }
  onGoogleLoginError(err) {
    console.log(err);
  }


  onLoginSuccess(res: FacebookLoginResponse) {
    const credential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
    this.afAuth.signInWithCredential(credential)
      .then((response: auth.UserCredential) => {
        console.log('after login');
        this.afterLogin(response);

      });

  }
  onLoginError(err) {
    console.log(err);
  }

  async loginWFacebookWEB() {


    await this.afAuth.signInWithRedirect(new auth.FacebookAuthProvider());
    // await this.afAuth.signInWithRedirect(new auth.FacebookAuthProvider());

    // console.log(val);
    // await this.afterLogin(val);
  }

  async afterLogin(response) {
    console.log('after login', response);
    const user = {} as UserLogin;

    user.email = (response.user.email === '' || response.user.email === null) ?
      (response.additionalUserInfo.profile.id === '' || response.additionalUserInfo.profile.id === null ?
        response.user.uid : response.additionalUserInfo.profile.id)
      : response.user.email;
    const userdata: any = await this.rest.getUserByEmail(user);

    if (userdata.data !== null) {
      user.password = response.user.uid;
      await this.loginAndNavigate(user);
    } else {
      user.password = response.user.uid;
      const ret: any = await this.rest.register(user);
      if (ret.status === true) {
        await this.loginAndNavigate(user, true);
      } else {
        this.alertService.presentToast('A problem occured when registering.');
        this.dismissLogin();
      }
    }
  }


  private async loginAndNavigate(user: UserLogin, firsttime = false, guest = false) {
    const loggedin: any = await this.rest.login(user, guest);
    if (loggedin.status === true) {
      this.alertService.presentToast('Logged In');
      this.shared.loggedin.next(true);
      this.dismissLogin();
      if (firsttime && !guest) {
        this.rest.earnPoints(100, 2);
      }
      this.router.navigateByUrl(this.tab1Str);
    }
  }

  async presentLoading() {
    this.isLoading = true;
    return await this.loadingController.create({
      duration: 5000,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  showBannerAd() {
    const bannerConfig: AdMobFreeBannerConfig = {
      autoShow: false,
      id: 'ca-app-pub-3709541018243663/6708998213'
    };
    this.admobFree.banner.config(bannerConfig);

    this.admobFree.banner.prepare().then(() => {
      // success
    }).catch(e => alert(e));
  }

  async loginAsGuest() {
    const user = {} as UserLogin;
    user.email = this.device.uuid;
    const userdata: any = await this.rest.getUserByEmail(user);

    if (userdata.data !== null) {
      user.password = this.device.serial;
      await this.loginAndNavigate(user, true, true);
    } else {
      user.password = this.device.serial;
      const ret: any = await this.rest.register(user, true);
      if (ret.status === true) {
        await this.loginAndNavigate(user, true, true);
      } else {
        this.alertService.presentToast('A problem occured when registering.');
        console.log(ret);
        this.dismissLogin();
      }
    }
  }
}
