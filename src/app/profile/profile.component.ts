import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { RestService } from '../services/rest.service';
import { ModalController, AlertController, ToastController } from '@ionic/angular';
import { SharedService } from '../services/shared.service';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})

export class ProfileComponent implements OnInit {
  user: any;

  constructor(private rest: RestService,
              private modalController: ModalController,
              private sharedService: SharedService,
              private toastController: ToastController,
              private translate: TranslateService,
              private ref: ChangeDetectorRef,
              private globalization: Globalization) {

   }

  ngOnInit() {
    this.user = this.rest.loggedInUser['account'];
    console.log(this.user);
  }

  dismiss() {
    this.modalController.dismiss();
  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }

  async updateUserName(){
    this.sharedService.presentLoading();
    await this.rest.updateUserName(this.user.userName);
    this.rest.loggedInUser['account'].userName = this.user.userName;
    this.presentToast(this.translate.instant('Username is updated'));
    this.sharedService.dismissLoading();
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
