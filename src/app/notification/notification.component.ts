import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { RestService } from '../services/rest.service';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../services/shared.service';
import { Globalization } from '@ionic-native/globalization/ngx';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent implements OnInit {

  @Input() notifications: any = [];
  constructor(private rest: RestService,
              private modalController: ModalController,
              private sharedService: SharedService,
              private translate: TranslateService,
              private ref: ChangeDetectorRef,
              private globalization: Globalization
  ) { }

  async ngOnInit(): Promise<void> {
    this.notifications.forEach(not => {
      this.rest.readNotification(not.ID);
    });
  }

  dismiss() {
    this.modalController.dismiss();
  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }


}
