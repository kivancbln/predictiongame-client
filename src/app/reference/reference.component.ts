import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RestService } from '../services/rest.service';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../services/shared.service';
import { AlertService } from '../services/alert.service';


@Component({
  selector: 'app-reference',
  templateUrl: './reference.component.html',
  styleUrls: ['./reference.component.scss'],
})
export class ReferenceComponent implements OnInit {
  reference: number;


  constructor(
    private rest: RestService,
    private modalController: ModalController,
    private ref: ChangeDetectorRef,
    private translate: TranslateService,
    private globalization: Globalization,
    private shared: SharedService,
    private alertService: AlertService
  ) { }

  ngOnInit() { }

  setReference(val) {
    this.reference = parseInt(val, 10);
  }

  async sendReference() {
    const userId = this.rest.loggedInUser['account'].ID;
    if (this.reference === userId) {
      await this.shared.presentAlert('Denied', 'You can not enter your user id.');
      return;
    }
    await this.rest.sendReference(this.reference);
    this.rest.loggedInUser['account'].ReferenceUserID = this.reference;
    await this.alertService.presentToast('Your reference is saved.');
    this.dismiss();
  }

  dismiss() {
    this.modalController.dismiss();
  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }
}
