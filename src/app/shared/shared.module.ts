import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatchCardComponent } from '../match-card/match-card.component';
import { IonicModule } from '@ionic/angular';
import { ChallengeBetComponent } from '../challenge-bet/challenge-bet.component';
import { DateFilterComponent } from '../date-filter/date-filter.component';
import { ExpandableComponent } from '../expandable/expandable.component';
import { FadeComponent } from '../fade/fade.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../app.module';
import { HttpClient } from '@angular/common/http';
import { BetPageComponent } from '../bet-page/bet-page.component';
import { RankingComponent } from '../ranking/ranking.component';
import { FormsModule } from '@angular/forms';
import { ProfileComponent } from '../profile/profile.component';
import { ActivePredictionsComponent } from '../active-predictions/active-predictions.component';
import { NotificationComponent } from '../notification/notification.component';
import { BuyableCardComponent } from '../buyable-card/buyable-card.component';
import { ReferenceComponent } from '../reference/reference.component';
import { TellafriendComponent } from '../tellafriend/tellafriend.component';



@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TranslateModule
  ],
  declarations: [
    MatchCardComponent,
    ChallengeBetComponent,
    BetPageComponent,
    DateFilterComponent,
    ExpandableComponent,
    FadeComponent,
    RankingComponent,
    ProfileComponent,
    ActivePredictionsComponent,
    NotificationComponent,
    BuyableCardComponent,
    ReferenceComponent,
    TellafriendComponent
  ],
  exports: [
    MatchCardComponent,
    ChallengeBetComponent,
    BetPageComponent,
    DateFilterComponent,
    ExpandableComponent,
    FadeComponent,
    RankingComponent,
    ProfileComponent,
    ActivePredictionsComponent,
    NotificationComponent,
    BuyableCardComponent,
    ReferenceComponent,
    TellafriendComponent,
    TranslateModule
  ]
})
export class SharedModule { }
