import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, HostListener } from '@angular/core';
import { BetPageComponent } from '../bet-page/bet-page.component';
import { RestService } from '../services/rest.service';
import { ModalController, ToastController, PickerController, AlertController } from '@ionic/angular';
import { SharedService, ActiveAd } from '../services/shared.service';
import { interval } from 'rxjs';
import { map } from 'rxjs/operators';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { AdMobFree, AdMobFreeRewardVideoConfig } from '@ionic-native/admob-free/ngx';

@Component({
  selector: 'app-match-card',
  templateUrl: './match-card.component.html',
  styleUrls: ['./match-card.component.scss'],
})
export class MatchCardComponent implements OnInit {
  @Input() disabled = false;
  @Input() entered = true;
  @Input() match: any;
  @Input() points: any;
  @Input() challengeId: any;
  @Input() challengeType = '';
  @Input() challengeMaxPoint = -1;
  @Input() challengeAllPoints = -1;
  @Output() changePoints: EventEmitter<number> = new EventEmitter<number>();
  betHome: any;
  betAway: any;
  betPoints: any;

  oddId: any;
  constructor(
    private readonly rest: RestService,
    public modalController: ModalController,
    private readonly toastController: ToastController,
    private readonly sharedService: SharedService,
    private readonly ref: ChangeDetectorRef,
    private readonly globalization: Globalization,
    private readonly translate: TranslateService,
    private readonly shared: SharedService,
    private readonly pickerController: PickerController,
    private readonly alertController: AlertController,
    private readonly admobFree: AdMobFree) { }

  ngOnInit() {
    this.setCountdown();
  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }

  async openBetPage() {

    if (this.entered) {

      this.sharedService.presentLoading();
      const pts = await this.sharedService.getPoints();

      const modal = await this.modalController.create({
        component: BetPageComponent,
        componentProps: {
          match: this.match,
          points: pts,
          modalCtrl: this.modalController,
          disabled: this.disabled,
          challengeId: this.challengeId,
          entered: this.entered,
          challengeAllPoints: this.challengeAllPoints,
          challengeMaxPoint: this.challengeMaxPoint
        }
      });

      modal.onDidDismiss().then(data => {
        this.refreshMatch();
        this.changePoints.emit(this.match);
      });
      this.sharedService.dismissLoading();
      return await modal.present();
    } else {

      this.presentToast('You need to enter challenge first');


    }
  }
  async refreshMatch() {
    this.match = (await this.rest.getMatchById(this.match.ID))[0];


    if (this.challengeId !== undefined) {
      const idxs = [];
      this.match.Bets.forEach(bet => {
        if (bet.challengeId !== this.challengeId) {
          idxs.push(this.match.Bets.indexOf(bet));
        }
      });
      idxs.forEach(idx => {
        this.match.Bets.splice(idx, 1);
      });
    }
    this.setCountdown();
  }

  setCountdown() {

    const future = new Date(this.match.date);
    const counter$ = interval(1000).pipe(map(() => {
      return Math.floor((future.getTime() - new Date().getTime()) / 1000);
    }));

    counter$.subscribe((x) => {
      this.match.countdown = this.sharedService.dhms(x);
      this.match.countdownNum = x;
      this.ref.detectChanges();
    });
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  getColorOfMatch(date, finished: boolean) {
    const newDate = date;
    const now = new Date();
    if (newDate > now) {
      return 'primary';
    } else if (newDate < now && !finished) {
      return 'secondary';
    }
  }

  getMatchDate(date) {
    return (new Date(date)).toLocaleString();
  }

  getMatchCountdown(countdownNum, countdown) {
    return countdownNum < 0 ? 'Started' : countdown;
  }

  getMatchStatusString() {
    if (this.match.status === 'PST') {
      return 'Postponed';
    } else if (this.match.status === 'CANC') {
      return 'Canceled';
    } else if (this.match.status === 'FT') {
      return 'Finished';
    }
  }

  async bet(factor = 1) {
    try {
      this.alertController.create();
      this.shared.presentLoading();
      const now = new Date();
      const matchdatenumeric = new Date(this.match.date);
      if (now > matchdatenumeric) {
        this.shared.dismissLoading();
        await this.presentToast(this.translate.instant('Match is already started'));
        return;
      }
      const bet: any = {};
      bet.userId = await this.rest.getUserId();
      bet.matchId = this.match.ID;
      bet.points = parseInt(this.betPoints.toString(), 10);
      bet.challengeId = this.challengeId;
      bet.oddId = this.oddId;
      bet.factor = factor;
      this.points = await this.sharedService.getPoints();
      if (this.points - bet.points < 0) {
        this.shared.dismissLoading();
        this.presentToast(this.translate.instant('Insufficient points'));
      } else {
        await this.rest.createBetByUserByMatch(bet);
        this.presentToast(this.translate.instant('Prediction is saved'));
        await this.sharedService.getPoints();
        await this.refreshMatch();
        this.changePoints.emit(this.match);
        this.shared.dismissLoading();
      }
    } catch (error) {
      console.log('error', error);
    } finally {
      this.shared.dismissLoading();

    }

  }

  async presentConfirm() {

    if (this.challengeMaxPoint > -1 && this.challengeAllPoints > -1) {
      if (this.challengeAllPoints + this.betPoints > this.challengeMaxPoint) {
        await this.presentToast(this.translate.instant('Spent: ')
          + this.challengeAllPoints + '\n' + this.translate.instant('Allowed: ' + this.challengeMaxPoint));
        return;
      }
    }


    const alert = await this.alertController.create({
      message: this.translate.instant('Would like to double your earning by watching video?'),
      buttons: [
        {
          text: this.translate.instant('No'),
          role: 'cancel',
          handler: async () => {
            await this.bet();
          }
        },
        {
          text: this.translate.instant('Yes'),
          handler: async () => {
            await this.openRewardedVideo();
          }
        }
      ]
    });
    await alert.present();
  }
  async openRewardedVideo() {
    try {
      ActiveAd.adid = this.match.ID;
      const RewardVideoConfig: AdMobFreeRewardVideoConfig = {
        // isTesting: true, // Remove in production
        autoShow: true, // ,
        id: 'ca-app-pub-3709541018243663/5816535635'
      };
      this.admobFree.rewardVideo.config(RewardVideoConfig);
      this.sharedService.presentLoading();
      const res = await this.admobFree.rewardVideo.prepare();

    } catch (e) {
      alert(e);
    }
  }

  async openPicker(oddval: string) {
    const now = new Date();
    const matchdatenumeric = new Date(this.match.date);
    if (now > matchdatenumeric) {
      await this.presentToast(this.translate.instant('Match is already started'));
      return;
    }

    if (!this.entered) {
      await this.presentToast('You need to enter challenge first');
      return;
    }
    const columns = [];


    const pointsOptions = [];
    for (let i = 1; i <= 10; i++) {
      pointsOptions.push({
        text: i * 5,
        value: i * 5
      });
    }

    columns.push({
      name: `col-1`,
      options: pointsOptions
    });

    const picker = await this.pickerController.create({
      columns,
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          role: 'cancel'
        },
        {
          text: this.translate.instant('Confirm'),
          handler: (value) => {
            this.oddId = this.getOddId(oddval);
            this.betPoints = value['col-1'].value;
            this.presentConfirm();
          }
        }
      ]
    });

    await picker.present();
  }

  getColumns(numColumns, numOptions, columnOptions, columnDescs) {
    const columns = [];
    const prefixes = [this.translate.instant('H'),
    this.translate.instant('A'),
    this.translate.instant('pts')];
    for (let i = 0; i < numColumns; i++) {
      columns.push({
        name: `col-${i}`,
        prefix: prefixes[i],
        options: this.getColumnOptions(i, numOptions, columnOptions, columnDescs)
      });
    }

    return columns;
  }

  getColumnOptions(columnIndex, numOptions, columnOptions, columnDescs) {
    const options = [];
    for (let i = 0; i < numOptions; i++) {
      options.push({
        text: columnOptions[columnIndex][i % numOptions],
        value: columnDescs[columnIndex][i % numOptions]
      });
    }

    return options;
  }

  @HostListener('document:admob.rewardvideo.events.REWARD', ['$event'])
  async rewardEnter(event: any) {
    console.log('reward catched');
    console.log(ActiveAd.adid);
    console.log(event);
    if (ActiveAd.adid === this.match.ID && event.returnValue) {
      console.log('entered bet 2');
      await this.bet(2);
      ActiveAd.adid = '';
    }
  }

  @HostListener('document:admob.rewardvideo.events.CLOSE', ['$event'])
  async closeEnter(event: any) {

    if (ActiveAd.adid === this.match.ID && !event.returnValue) {
      alert('video closed');
      this.sharedService.dismissLoading();
      await this.bet();
      ActiveAd.adid = '';
    }
  }

  @HostListener('document:admob.rewardvideo.events.LOAD_FAIL', ['$event'])
  async loadFailEnter(event: any) {
    if (ActiveAd.adid === this.match.ID) {
      alert('video load failed');
      this.sharedService.dismissLoading();
      await this.bet();
      ActiveAd.adid = '';
    }
  }

  @HostListener('document:admob.rewardvideo.events.EXIT_APP', ['$event'])
  async exitAppEnter(event: any) {
    this.sharedService.dismissLoading();
  }

  checkESVisible() {
    return this.challengeType === '' ? true : (this.challengeType === 'Exact Score' ? true : false);
  }

  checkMWVisible() {
    return this.challengeType === '' ? true : (this.challengeType === 'Match Winner' ? true : false);
  }

  getOdd(val) {
    const obj = this.match.MatchOdds.find(x => x.oddDesc === 'Match Winner' && x.oddValue === val);
    if (obj !== undefined) {
      return obj.rate;
    } else {
      return 0;
    }
  }

  getOddId(val) {
    const obj = this.match.MatchOdds.find(x => x.oddDesc === 'Match Winner' && x.oddValue === val);
    if (obj !== undefined) {
      return obj.ID;
    } else {
      return 0;
    }
  }

  getOddCount(){
    return this.match.MatchOdds.filter(x=>x.oddDesc !== 'Match Winner').length;
  }


}
