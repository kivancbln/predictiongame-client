import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { RestService } from '../services/rest.service';
import { interval, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SharedService } from '../services/shared.service';
import { ChallengeBetComponent } from '../challenge-bet/challenge-bet.component';
import { ModalController } from '@ionic/angular';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { AdMobFreeInterstitialConfig, AdMobFree } from '@ionic-native/admob-free/ngx';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  userid: number;
  challenges: any[];
  isactive = true;
  finisheds: any;
  interstitialShown = false;
  page = 1;
  offset = 15;
  finishedPage = 1;
  finishedOffset = 15;


  constructor(private rest: RestService,
    private sharedService: SharedService,
    public modalController: ModalController,
    private ref: ChangeDetectorRef,
    private globalization: Globalization,
    private translate: TranslateService,
    private admobFree: AdMobFree) { }

  async ngOnInit(): Promise<void> {

    this.sharedService.presentLoading();
    this.userid = await this.rest.getUserId();
    await this.setChallenges();
    this.showInterstitialAds();
    if (!this.interstitialShown) {
      await this.admobFree.interstitial.prepare();
    }

    this.admobFree.on(this.admobFree.events.INTERSTITIAL_LOAD).subscribe(() => {
      this.admobFree.interstitial.show().then(() => {
        this.interstitialShown = true;
      }).catch((errorShow) => {
        console.log('interstitial error', errorShow);
      });
    });

    this.sharedService.dismissLoading();
  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }

  setCountdown() {
    this.challenges.forEach((challenge): any => {
      const future: Date = new Date(challenge.endDate);
      const counter$ = interval(1000).pipe(map((x) => {
        return Math.floor((future.getTime() - new Date().getTime()) / 1000);
      }));

      this.subscribeCounter(counter$, challenge);
    });

    this.finisheds.forEach((challenge): any => {
      const future: Date = new Date(challenge.endDate);
      const counter$ = interval(1000).pipe(map((x) => {
        return Math.floor((future.getTime() - new Date().getTime()) / 1000);
      }));

      this.subscribeCounter(counter$, challenge);
    });
  }

  setCountdownById(challengeid: number) {
    const challenge: any = this.challenges.filter(x => x.ID === challengeid);
    const future: Date = new Date(challenge.endDate);
    const counter$ = interval(1000).pipe(map((x) => {
      return Math.floor((future.getTime() - new Date().getTime()) / 1000);
    }));
    this.subscribeCounter(counter$, challenge);

  }
  subscribeCounter(counter$: Observable<number>, challenge: any) {
    counter$.subscribe((x) => {
      challenge.countdown = this.sharedService.dhms(x);
      challenge.countdownNum = x;
      challenge.started = new Date(challenge.startDate) < new Date();
      this.ref.detectChanges();

    });
  }



  getChallengeCountdown(countdownNum, countdown, startDate) {

    return new Date(startDate) > new Date() ? new Date(startDate).toLocaleString() : countdown;
  }

  doRefresh(event): Promise<any> {
    return new Promise((resolve) => {
      setTimeout(async () => {
        this.setChallenges();
        event.target.complete();
        this.ref.detectChanges();
        resolve();
      }, 1000);
    });
  }

  async openChallengeDet(index: number, isactive: boolean) {
    const modal = await this.modalController.create({
      component: ChallengeBetComponent,
      componentProps: {
        challenge: isactive ? this.challenges[index] : this.finisheds[index],
        modalCtrl: this.modalController
      }
    });

    modal.onDidDismiss().then(data => {
      console.log(data.data);
      this.setChallengeById(data.data.challengeid, isactive);

    });

    return await modal.present();
  }



  async setChallenges() {

    this.challenges = await this.rest.getChallengesPaged(this.userid, this.page, this.offset);

    this.challenges.forEach((challenge): any => {
      this.setChallengeProps(challenge);
    });

    this.finisheds = await this.rest.getFinishedChallengesPaged(this.userid, this.finishedPage, this.finishedOffset);
    this.finisheds.forEach(fin => {
      this.setChallengeProps(fin);
    });
    this.setCountdown();
    this.ref.detectChanges();
  }

  setChallengeProps(challenge: any) {
    let matchCount = 0;
    let predictedCount = 0;

    challenge.predicted = challenge.Matches.reduce((pn, u) => [...pn, ...u.Match.Bets], []).length > 0;
    challenge.Matches.forEach(match => {
      matchCount++;
      if (match.Match.Bets.length > 0) {
        predictedCount++;
      }
    });
    challenge.matchCount = matchCount;
    challenge.predictedCount = predictedCount;
  }

  async setChallengeById(challengeid: number, isactive: boolean) {

    const challenge = (await this.rest.getChallengeById(this.userid, challengeid))[0];

    let matchCount = 0;
    let predictedCount = 0;
    challenge.predicted = challenge.Matches.reduce((pn, u) => [...pn, ...u.Match.Bets], []).length > 0;
    challenge.Matches.forEach(match => {
      matchCount++;
      if (match.Match.Bets.length > 0) {
        predictedCount++;
      }
    });
    challenge.matchCount = matchCount;
    challenge.predictedCount = predictedCount;
    if (isactive) {
      const oldch = this.challenges.filter(x => x.ID === challenge.ID)[0];
      const updix = this.challenges.indexOf(oldch);
      this.challenges[updix] = challenge;
    } else {
      const oldch = this.finisheds.filter(x => x.ID === challenge.ID)[0];
      const updix = this.finisheds.indexOf(oldch);
      this.finisheds[updix] = challenge;
    }
    // this.setCountdownById(challengeid);
    this.setCountdown();
    this.ref.detectChanges();
  }

  segmentChanged(e) {
    this.isactive = e.detail.value === 'active';
    this.ref.detectChanges();
  }

  showInterstitialAds() {
    const interstitialConfig: AdMobFreeInterstitialConfig = {
      autoShow: false,
      id: 'ca-app-pub-3709541018243663/4744894440'
    };
    this.admobFree.interstitial.config(interstitialConfig);
    this.admobFree.interstitial.prepare().then(() => {
    }).catch(e => alert(e));
  }

  doInfinite(event): Promise<any> {


    return new Promise((resolve) => {
      setTimeout(async () => {
        this.page = this.page + 1;
        const userid = await this.rest.getUserId();
        const trs = await this.rest.getChallengesPaged(userid, this.page, this.offset);
        if (trs.length > 0) {
          trs.forEach((challenge): any => {
            this.setChallengeProps(challenge);
          });
          for (const tr of trs) {
            this.challenges.push(tr);
          }
          this.setCountdown();
          event.target.complete();
        } else {
          event.target.disabled = true;
        }


        resolve();
      }, 1000);
    });
  }

  doInfiniteFinished(event): Promise<any> {


    return new Promise((resolve) => {
      setTimeout(async () => {
        this.finishedPage = this.finishedPage + 1;
        const userid = await this.rest.getUserId();
        const trs = await this.rest.getFinishedChallengesPaged(userid, this.finishedPage, this.finishedOffset);
        if (trs.length > 0) {
          trs.forEach((challenge): any => {
            this.setChallengeProps(challenge);
          });
          for (const tr of trs) {
            this.finisheds.push(tr);
          }
          this.setCountdown();
          event.target.complete();
        } else {
          event.target.disabled = true;
        }


        resolve();
      }, 1000);
    });
  }

}
