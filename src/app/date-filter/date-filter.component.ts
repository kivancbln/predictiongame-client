import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-date-filter',
  templateUrl: './date-filter.component.html',
  styleUrls: ['./date-filter.component.scss'],
})
export class DateFilterComponent implements OnInit {

  @Input() current: Date;

  constructor(
    private readonly modalCtrl: ModalController,
    private readonly ref: ChangeDetectorRef,
    private readonly globalization: Globalization,
    private readonly translate: TranslateService) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }

  getDate(days: number) {
    const date = new Date();
    date.setDate(date.getDate() + days);
    return date.toISOString().split('T')[0];
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      filter: this.current
    });
  }

  setCurrent(days: number) {
    const date = new Date();
    date.setDate(date.getDate() + days);
    this.current = date;
  }

  selectDate(e) {
    this.current = (new Date(e.detail.value.split('T')[0]));
  }

  ifCustom() {
    const startdate = new Date();
    startdate.setDate(startdate.getDate() + -1);
    const enddate = new Date();
    enddate.setDate(enddate.getDate() + 3);
    return this.current > enddate || this.current < startdate;
  }

}
