import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Facebook } from '@ionic-native/facebook/ngx'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BetPageComponent } from './bet-page/bet-page.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { FormsModule } from '@angular/forms';
import { IonicStorageModule } from '@ionic/storage';
import { LandingPage } from './landing/landing.component';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Toast } from '@ionic-native/toast/ngx';
import { SocketService } from './services/socket.service';
import { ChallengeBetComponent } from './challenge-bet/challenge-bet.component';
import { SharedModule } from './shared/shared.module';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdMobFree } from '@ionic-native/admob-free/ngx';
import { PointsClass } from './services/shared.service';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { Globalization } from '@ionic-native/globalization/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Device } from '@ionic-native/device/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent, LoginComponent, RegisterComponent, LandingPage],
  entryComponents: [BetPageComponent, ChallengeBetComponent],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule,
            FormsModule, SharedModule, IonicStorageModule.forRoot(),
            AngularFireModule.initializeApp(environment.config),
            AngularFireAuthModule, BrowserAnimationsModule,
            TranslateModule.forRoot({
              loader: {
                  provide: TranslateLoader,
                  useFactory: HttpLoaderFactory,
                  deps: [HttpClient]
              }
          })],
  providers: [
    Toast,
    StatusBar,
    SplashScreen,
    SocketService,
    Facebook,
    GooglePlus,
    AdMobFree,
    PointsClass,
    Globalization,
    Device,
    OneSignal,
    Clipboard,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
