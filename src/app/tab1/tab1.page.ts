import {
  Component, AfterViewInit, OnInit, ChangeDetectorRef, Renderer2, ViewChild, ElementRef,
  NgZone, AfterContentInit
} from '@angular/core';
import { RestService } from '../services/rest.service';
import { ModalController, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { BetPageComponent } from '../bet-page/bet-page.component';
import { Toast } from '@ionic-native/toast/ngx';
import { LoginResponse } from '../interfaces/login-response';
import { SharedService, PointsClass } from '../services/shared.service';
import { interval, of, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { VERSION } from '../../environments/version';
import { DateFilterComponent } from '../date-filter/date-filter.component';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';
import { AdMobFreeInterstitialConfig, AdMobFree } from '@ionic-native/admob-free/ngx';
import { Device } from '@ionic-native/device/ngx';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements AfterViewInit, OnInit, AfterContentInit {
  @ViewChild('activescroll') activescroll: ElementRef;
  @ViewChild('finishedscroll') finishedscroll: ElementRef;

  version = VERSION;
  matches: any = [];
  user: LoginResponse = { ID: 0, email: '', token: '', guest: false, uuid: '', model: '', serial: '', points: 0 };
  points: any;
  isactive = true;
  finisheds: any;
  filterFlag = false;
  originalmatches: any;
  originalfinisheds: any;
  filteredLeagues: any[] = [];
  originalAllMatches: any[];
  teamFilterFlag = false;
  dateFilter = new Date((new Date(new Date().toDateString()).getTime()
    - new Date(new Date().toDateString()).getTimezoneOffset() * 60000));
  screenHeight: any;
  leagues: any;
  activeLeagues: any[] = [];
  page: number;
  offset: number;
  finishedLeagues: any[] = [];
  activeFinishedLeagues: any;
  filterGroupFlag = false;
  teamFilter = '';
  activeLeagueCountChanges$ = new Subject();
  finishedLeagueCountChanges$ = new Subject();
  isLoading: boolean;
  interstitialShown = false;
  uuidUserData: any;
  notifications: any;




  constructor(
    private rest: RestService,
    public modalController: ModalController,
    private toastController: ToastController,
    private sharedService: SharedService,
    private ref: ChangeDetectorRef,
    private renderer: Renderer2,
    public pointsclass: PointsClass,
    public loadingController: LoadingController,
    private zone: NgZone,
    public translate: TranslateService,
    private globalization: Globalization,
    private admobFree: AdMobFree,
    private device: Device,
    public alertController: AlertController) {

  }
  async ngAfterContentInit(): Promise<void> {

    await this.admobFree.banner.hide();

  }

  activeLeagueCount() {
    return this.activeLeagues.filter(x => x.matches.some(y => y.visible)).length;
  }

  finishedLeagueCount() {
    return this.finishedLeagues.filter(x => x.matches.some(y => y.visible)).length;
  }
  async ionViewDidEnter(): Promise<void> {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });


  }

  async openBetPage(index: number, isactive: boolean) {
    const modal = await this.modalController.create({
      component: BetPageComponent,
      componentProps: {
        match: this.matches[index],
        points: this.points,
        modalCtrl: this.modalController
      }
    });

    modal.onDidDismiss().then(data => {
      if (data.data.dismissed === true) {
        this.presentToast(this.translate.instant('Prediction is saved'));
      }
      this.getPoints();
      this.rest.setTransactions();

    });

    return await modal.present();
  }
  async ngAfterViewInit(): Promise<void> {
  }

  async ngOnInit(): Promise<void> {
    this.isactive = true;
    this.getValues().then(() => {
      this.ref.detectChanges();
    });

    this.screenHeight = (screen.height / 3).toString().concat('px');
    this.offset = 15;

    /*this.matches = await this.rest.getMatches();
    this.finisheds = await this.rest.getFinishedMatches();
    this.user = await this.rest.getUser();

    await this.getPoints();
    await this.rest.setTransactions();
    this.setCountdown();*/

  }

  private async getValues() {
    this.presentLoading();
    const dateFilterGMT = new Date((this.dateFilter.getTime() + this.dateFilter.getTimezoneOffset() * 60000));
    const matchPromise = this.rest.getMatchesByDate(dateFilterGMT.toISOString());
    const finishedPromies = this.rest.getFinishedMatchesByDate(dateFilterGMT.toISOString());
    const pointsPromise = this.getPoints();
    const coinsPromise = this.sharedService.getCoins();
    const uuidcheck = this.rest.getUserByUUID(this.device.uuid);
    const promises = [matchPromise, finishedPromies, pointsPromise, uuidcheck, coinsPromise];
    Promise.allSettled(promises).
      then((results) => results.forEach((result: any, index) => {
        if (index === 0) {
          this.originalmatches = result.value;
          this.matches = result.value;
        } else if (index === 1) {
          this.originalfinisheds = result.value;
          this.finisheds = result.value;
        } else if (index === 3) {
          this.uuidUserData = result.value;
        }
      })).then(res => {
        // this.originalmatches = await this.rest.getMatchesByDate(dateFilterGMT.toISOString());
        // this.matches = JSON.parse(JSON.stringify(this.originalmatches));
        // this.originalfinisheds = await this.rest.getFinishedMatchesByDate(dateFilterGMT.toISOString()); ;
        // this.finisheds = JSON.parse(JSON.stringify(this.originalfinisheds));
        this.originalAllMatches = [...this.originalmatches, ...this.originalfinisheds];
        // await this.getPoints();


        this.matches.forEach(m => {
          m.visible = true;
        });

        this.finisheds.forEach(m => {
          m.visible = true;
        });

        this.leagues = this.getLeaguesOfMatches(this.originalAllMatches);
        this.finishedLeagues = JSON.parse(JSON.stringify(this.leagues));

        this.leagues.forEach(league => {
          league.matches = this.getMatchesOfLeague(league, this.matches);
          this.finishedLeagues.find(x => x.leagueId === league.leagueId).matches = this.getMatchesOfLeague(league, this.finisheds);
        });

        /*this.finishedLeagues.forEach(league2 => {
          league2.matches = this.getMatchesOfLeague(league2, this.finisheds);
        });*/

        this.page = 0;

        this.originalAllMatches = null;
        this.originalfinisheds = null;
        this.matches = null;
        this.finisheds = null;

        this.activeLeagues = this.leagues.filter(x => x.matches.length > 0)
          .slice(this.page * this.offset, this.page * this.offset + this.offset);
        this.activeFinishedLeagues = this.finishedLeagues.filter(x => x.matches.length > 0)
          .slice(this.page * this.offset, this.page * this.offset + this.offset);

        this.activeLeagueCountChanges$.subscribe(actChange => {
          if (actChange !== undefined) {
            this.loadMoreLeagues(this.activescroll);
          }
        });

        this.finishedLeagueCountChanges$.subscribe(finChange => {
          if (finChange !== undefined) {
            this.loadMoreFinishedLeagues(this.finishedscroll);
          }
        });

        this.dismissLoading();
        this.ref.detectChanges();

        const usr: any = this.rest.loggedInUser;
        if (usr.account.guest !== true && this.uuidUserData.data !== null && this.uuidUserData.data !== undefined) {
          this.presentUUIDAlert();
        }


      });


  }

  async presentUUIDAlert() {
    const usr: any = this.rest.loggedInUser;
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: this.translate.instant('Alert'),
      subHeader: this.translate.instant('Account merge'),
      message: this.translate.instant('A guest account already exists for this device: ') +
        this.device.model +
        this.translate.instant(' If you want to merge two accounts into one click Yes, then guest account will be deleted, ' +
          'if you click No guest account will also be deleted but you will lose the progress you have made'),
      buttons: [
        {
          text: this.translate.instant('NO'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.rest.confirmOrCancelGuest(this.device.uuid, usr.account.ID, 0);
          }
        }, {
          text: this.translate.instant('YES'),
          handler: () => {
            this.rest.confirmOrCancelGuest(this.device.uuid, usr.account.ID, 1);
          }
        }
      ]
    });


    await alert.present();
  }

  setCountdown() {
    this.matches.forEach(match => {
      const future = new Date(match.date);
      const counter$ = interval(1000).pipe(map((x) => {
        return Math.floor((future.getTime() - new Date().getTime()) / 1000);
      }));

      counter$.subscribe((x) => { match.countdown = this.sharedService.dhms(x); match.countdownNum = x; });
    });
  }
  async getPoints() {

    this.points = await this.sharedService.getPoints();
    this.matches.forEach(match => {
      match.betCount = 0;
    });

    const bets: any[] = await this.rest.getPreviousBetsByUser(this.user.ID);

    bets.filter(x => x.DeletedAt === null).forEach(bet => {
      this.matches.filter(x => x.ID === bet.matchId).forEach(match => {
        match.betCount = (match.betCount || 0) + 1;
      });
    });
  }

  async pointsChange(e) {
    await this.getPoints();
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  getColorOfMatch(date, finished: boolean) {
    const newDate = date;
    const now = new Date();
    if (newDate > now) {
      return 'primary';
    } else if (newDate < now && !finished) {
      return 'secondary';
    }
  }

  getMatchDate(date) {
    return (new Date(date)).toLocaleString();
  }

  getMatchCountdown(countdownNum, countdown) {
    return countdownNum < 0 ? 'Started' : countdown;
  }

  segmentChanged(e) {
    this.isactive = e.detail.value === 'active';
    this.ref.detectChanges();
  }

  toggleFilter() {
    this.zone.run(() => { this.filterFlag = !this.filterFlag; });
  }

  toggleTeamFilter() {
    this.zone.run(() => { this.teamFilterFlag = !this.teamFilterFlag; });
  }

  toggleFilterGroup() {
    this.zone.run(() => { this.filterGroupFlag = !this.filterGroupFlag; });
  }


  getLeagues() {
    let lgs = [];

    lgs = this.originalAllMatches.map(x => x.League);

    return lgs.filter((lg, i, arr) => arr.findIndex(t => t.ID === lg.ID) === i);

  }
  getLeagueName(lg) {
    const name = lg.ID > 0 ? lg.country.toString().concat(' - ').concat(lg.name) : 'Other';
    return name.concat(' (' + lg.matches.filter(x => x.visible).length + ')');
  }

  filterByLeague(lg, e) {

    if (lg.leagueId > -1) {
      if (this.filteredLeagues.some(x => x === lg)) {
        const ix = this.filteredLeagues.indexOf(lg);
        this.filteredLeagues.splice(ix, 1);
        this.renderer.removeStyle(
          e.target,
          'background'
        );

      } else {
        this.filteredLeagues.push(lg);
        this.renderer.setStyle(
          e.target,
          'background',
          'grey'
        );
      }
    }

    this.teamFilterChanged(this.teamFilter);

    this.ref.detectChanges();
  }

  isLeagueSelected(lg) {
    return this.filteredLeagues.some(x => x === lg);
  }

  clearAllLeagueFilters() {
    this.filteredLeagues = [];
    this.teamFilterChanged(this.teamFilter);

    this.ref.detectChanges();
  }

  teamFilterChanged(e) {

    this.teamFilter = e.toLocaleUpperCase();
    /*this.activeLeagues = this.leagues.filter(y =>
      y.matches.some((x => x.home.toString().toLocaleUpperCase().includes(this.teamFilter)
        || x.away.toString().toLocaleUpperCase().includes(this.teamFilter))
      )
      &&
      (this.filteredLeagues.some(fl => fl.leagueId === y.leagueId) || this.filteredLeagues.length === 0)
    );

    this.activeFinishedLeagues = this.finishedLeagues.filter(y =>
      y.matches.some(x => x.home.toString().toLocaleUpperCase().includes(this.teamFilter)
        || x.away.toString().toLocaleUpperCase().includes(this.teamFilter)
      )
      &&
      (this.filteredLeagues.some(fl => fl.leagueId === y.leagueId) || this.filteredLeagues.length === 0)
    );

    this.SetMatchesByTeamFilter();*/
    /*this.matches = this.leagues.filter(x => x.home.toString().toLocaleUpperCase().includes(e)
    || x.away.toString().toLocaleUpperCase().includes(e));
    this.finisheds = this.originalfinisheds.filter(x => x.home.toString().toLocaleUpperCase().includes(e)
    || x.away.toString().toLocaleUpperCase().includes(e));*/
    this.leagues.forEach(al => {
      al.matches.forEach(match => {
        match.visible = (match.home.toString().toLocaleUpperCase().includes(this.teamFilter)
          || match.away.toString().toLocaleUpperCase().includes(this.teamFilter)
          || this.teamFilter === '')
          &&
          (this.filteredLeagues.some(fl => fl.leagueId === match.leagueId) || this.filteredLeagues.length === 0);
      }
      );
    });

    this.finishedLeagues.forEach(al => {
      al.matches.forEach(match => {
        match.visible = (match.home.toString().toLocaleUpperCase().includes(this.teamFilter)
          || match.away.toString().toLocaleUpperCase().includes(this.teamFilter)
          || this.teamFilter === '')
          &&
          (this.filteredLeagues.some(fl => fl.leagueId === match.leagueId) || this.filteredLeagues.length === 0);
      }
      );
    });


    try {
      this.activeLeagueCountChanges$.next(this.activeLeagueCount());
      this.finishedLeagueCountChanges$.next(this.finishedLeagueCount());
    } catch (error) {

    }



    this.ref.detectChanges();

  }

  private SetMatchesByTeamFilter() {
    this.activeLeagues.forEach(league => {
      league.matches = this.leagues.find(x => x.leagueId === league.leagueId).matches
        .filter(x => x.home.toString().toLocaleUpperCase().includes(this.teamFilter)
          || x.away.toString().toLocaleUpperCase().includes(this.teamFilter) || this.teamFilter === '');
    });
    this.activeFinishedLeagues.forEach(league => {
      league.matches = this.finishedLeagues.find(x => x.leagueId === league.leagueId).matches
        .filter(x => x.home.toString().toLocaleUpperCase().includes(this.teamFilter)
          || x.away.toString().toLocaleUpperCase().includes(this.teamFilter) || this.teamFilter === '');
    });
  }

  async openDateFilter() {
    const modal = await this.modalController.create({
      component: DateFilterComponent,
      componentProps: {
        current: this.dateFilter
      }
    });

    modal.onDidDismiss().then(data => {
      this.dateFilter = data.data.filter;
      this.getValues().then(() => {
        this.ref.detectChanges();
      });


    });

    return await modal.present();
  }

  getLeaguesOfMatches(matches) {
    const lgs = matches.map(x => x.League);
    return lgs.filter((lg, i, arr) => arr.findIndex(t => t.ID === lg.ID) === i);
  }

  getMatchesOfLeague(league, matches) {
    return matches.filter(x => x.League.leagueId === league.leagueId);
  }

  expandCollapse(e, lg) {
    lg.expanded = !(lg.expanded || null || false);
    this.ref.detectChanges();
  }

  getExpandHeight(id) {

    return document.getElementById('lggrid'.concat(id)).style.height;
  }

  loadMoreLeagues(event) {

    const cnt = this.activeLeagueCount();
    return new Promise((resolve) => {
      setTimeout(async () => {
        this.page = this.page + 1;
        const lgs = this.leagues.filter(x => x.matches.length > 0).slice(this.page * this.offset, this.page * this.offset + this.offset);
        if (lgs.length > 0) {
          for (const lg of lgs) {
            this.activeLeagues.push(lg);
          }
          event.complete();
        } else {
          event.disabled = true;
        }

        this.ref.detectChanges();

        const cnt2 = this.activeLeagueCount();

        if (cnt === 0 && cnt2 === 0) {
          this.activeLeagueCountChanges$.next(cnt2);
        } else if (cnt > 0 && cnt2 === cnt && this.page * this.offset < this.leagues.length) {
          this.activeLeagueCountChanges$.next(cnt2);
        } else if (cnt > 0 && cnt2 < this.offset && this.page * this.offset < this.leagues.length) {
          this.activeLeagueCountChanges$.next(cnt2);
        }


        resolve();
      }, 1000);
    });
  }

  loadMoreFinishedLeagues(event) {
    const cnt = this.finishedLeagueCount();

    return new Promise((resolve) => {
      setTimeout(async () => {
        this.page = this.page + 1;
        const lgs = this.finishedLeagues
          .filter(x => x.matches.length > 0)
          .slice(this.page * this.offset, this.page * this.offset + this.offset);
        if (lgs.length > 0) {
          for (const lg of lgs) {
            this.activeFinishedLeagues.push(lg);
          }
          event.complete();
        } else {
          event.disabled = true;
        }

        this.ref.detectChanges();
        const cnt2 = this.finishedLeagueCount();
        if (cnt === 0 && cnt2 === 0) {
          this.finishedLeagueCountChanges$.next(cnt2);
        } else if (cnt > 0 && cnt2 === cnt && this.page * this.offset < this.finishedLeagues.length) {
          this.finishedLeagueCountChanges$.next(cnt2);
        } else if (cnt > 0 && cnt2 < this.offset && this.page * this.offset < this.finishedLeagues.length) {
          this.finishedLeagueCountChanges$.next(cnt2);
        }

        resolve();
      }, 1000);
    });
  }

  getPointsFromClass() {
    return PointsClass.points;
  }
  getCoinsFromClass() {
    return PointsClass.coins;
  }

  getVisibleMatchCount(lg) {
    return lg.matches.some(x => x.visible);
  }

  async presentLoading() {
    this.isLoading = true;
    return await this.loadingController.create({
      duration: 5000,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismissLoading() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }




  logout() {
    this.rest.logout();
  }

}
