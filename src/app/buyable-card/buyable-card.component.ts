import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { RestService } from '../services/rest.service';
import { SharedService } from '../services/shared.service';
import { ToastController, AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';


@Component({
  selector: 'app-buyable-card',
  templateUrl: './buyable-card.component.html',
  styleUrls: ['./buyable-card.component.scss'],
})
export class BuyableCardComponent implements OnInit {

  @Input() buyable;
  email = '';
  constructor(
    private readonly rest: RestService,
    private readonly sharedService: SharedService,
    private readonly toastController: ToastController,
    private readonly globalization: Globalization,
    private readonly translate: TranslateService,
    private readonly ref: ChangeDetectorRef,
    private readonly alertController: AlertController
  ) { }

  ngOnInit() { }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }

  async buy() {
    this.sharedService.presentLoading();
    let pts = await this.rest.getCoin();
    if (pts === undefined) {
      pts = 0;
    }
    if (parseInt(pts, 10) < parseInt(this.buyable.coin, 10)) {
      this.sharedService.dismissLoading();
      this.presentToast(this.translate.instant('Insufficient coins'));
    } else {
      const obj: any = {};
      obj.buyableId = this.buyable.ID;
      obj.userId = await this.rest.getUserId();
      obj.email = this.email;
      await this.rest.createBuyOrder(obj);
      await this.sharedService.getCoins();
    }

    this.sharedService.dismissLoading();
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  async openConfirm() {
    const alert = await this.alertController.create({
      message: this.translate.instant('Please write down your email address and click send.'),
      inputs: [
        {
          name: 'email',
          placeholder: 'email'
        }
      ],
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          role: 'cancel',
          handler: data => {
            this.presentToast(this.translate.instant('Cancelled.'));
          }
        },
        {
          text: this.translate.instant('Send'),
          handler: async (data) => {
            if (data.email !== undefined && data.email !== '' && data.email.includes('@')) {
              this.email = data.email;
              await this.buy();
            } else {
              this.presentToast(this.translate.instant('Please enter valid email address.'));
            }
          }
        }
      ]
    });
    await alert.present();
  }

}
