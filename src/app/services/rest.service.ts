import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserLogin } from '../interfaces/user-login';
import { LoginResponse } from '../interfaces/login-response';
import { Storage } from '@ionic/storage';
import { from, of, Subject, Observable } from 'rxjs';
import { Socket } from 'ngx-socket-io';
import * as io from 'socket.io-client';
import { Gain } from '../interfaces/gain';
import { Device } from '@ionic-native/device/ngx';

const endpoint = 'https://api.playpgame.com/api/';



@Injectable({
  providedIn: 'root'
})
export class RestService {
  

  accessToken: string;
  userid: number;
  loggedInUser: LoginResponse;
  transactions: { CreatedAt: string; home: any; away: any; points: any; type: any; }[];
  transactionChanges$ = new Subject();
  socket: SocketIOClient.Socket;
  constructor(
    private http: HttpClient,
    private router: Router,
    private storage: Storage,
    private device: Device) {

  }


  async getMatches() {

    try {
      const resp: any = await this.http.get(`${endpoint}match/list`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async getMatchesByDate(date: string) {

    try {
      const resp: any = await this.http.get(`${endpoint}match/listbydate?date=${date}`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async getFinishedMatches() {
    try {
      const resp: any = await this.http.get(`${endpoint}match/getfinishedmatches`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async getFinishedMatchesByDate(date: string) {

    try {
      const resp: any = await this.http.get(`${endpoint}match/getfinishedmatchesbydate?date=${date}`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async getMatchById(matchid: number) {
    try {
      const resp: any = await this.http.get(`${endpoint}match/getbyid?matchId=${matchid}`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async getPreviousBetsByUserByMatch(userid: number, matchid: number) {
    try {
      const resp: any = await this.http.get(`${endpoint}bet/list?userId=${userid}&matchId=${matchid}`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async getPreviousBetsByUser(userid: number) {
    try {
      const resp: any = await this.http.get(`${endpoint}bet/listbyuser?userId=${userid}`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async getActiveBets() {
    try {
      const resp: any = await this.http.get(`${endpoint}bet/getactivebets`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async getFinishedBets() {
    try {
      const resp: any = await this.http.get(`${endpoint}bet/getfinishedbets`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async createBetByUserByMatch(bet: any) {

    try {
      return this.http.post(`${endpoint}bet/new`, bet, {
        headers: {
          Authorization: 'Bearer ' + await this.getAccessToken(),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async login(data: UserLogin, guest = false) {
    try {
      const params: UserLogin = {
        email: data.email,
        password: data.password,
        uuid: this.device.uuid,
        serial: this.device.serial,
        model: this.device.model,
        guest
      };
      const resp: any = await this.http.post(`${endpoint}user/login`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
      this.loggedInUser = (await resp) as LoginResponse;
      this.storage.set('user', JSON.stringify(this.loggedInUser));
      // this.router.navigate(['/dashboard']);

      return resp;
    } catch (e) {
      console.log(e);
      alert(JSON.stringify(e));
    }
  }

  async register(data: UserLogin, guest = false) {
    try {
      const params: UserLogin = {
        email: data.email,
        password: data.password,
        uuid: this.device.uuid,
        serial: this.device.serial,
        model: this.device.model,
        guest
      };
      const resp: any = await this.http.post(`${endpoint}user/new`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
      return resp;
    } catch (e) {
      console.log(e);
    }
  }

  async getUser() {
    const userstring = await this.storage.get('user');
    const user: LoginResponse = JSON.parse(userstring).account;
    return user;
  }

  async getAccessToken() {
    const userstring = await this.storage.get('user');
    if (userstring === undefined || userstring === '' || userstring === null) {
      return '';
    }
    const user: LoginResponse = JSON.parse(userstring).account;

    return user.token;
  }

  async getUserId() {
    const userstring = await this.storage.get('user');
    this.loggedInUser = JSON.parse(userstring);
    if (userstring) {
      const user: LoginResponse = JSON.parse(userstring).account;
      return user.ID;
    } else {
      this.logout();
    }
  }

  async getPoints(userid: number) {
    try {
      const resp: any = await this.http.get(`${endpoint}gain/getpoints?userId=${userid}`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async deleteBet(betid) {
    try {
      const resp: any = await this.http.delete(`${endpoint}bet/delete/${betid.toString()}`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  getTransactions() {
    const observable$ = of(this.transactions);
    return observable$;
  }

  async setTransactions() {
    const user = await this.getUser();
    const points: any[] = await this.getPoints(user.ID);
    const bets: any[] = await this.getPreviousBetsByUser(user.ID);

    const newpoints = points.map(val =>
      ({ CreatedAt: (new Date(val.CreatedAt)).toLocaleString(),
        home: val.home, away: val.away, points: val.gainedPoints, type: val.type }));
    const newbets = bets.map(val =>
      ({ CreatedAt: (new Date(val.CreatedAt)).toLocaleString(),
        home: val.home, away: val.away, points: val.points, type: -1 }));


    newpoints.forEach(element => {
      if (element.type === 2) {
        element.home = 'Points load';
      }
    });

    newbets.forEach(element => {
      element.points = -1 * element.points;
    });

    this.transactions = [...newpoints, ...newbets].sort((a, b) => ((new Date(a.CreatedAt)) < (new Date(b.CreatedAt))) ? 1 : -1);
    this.transactionChanges$.next(this.transactions);
  }

  async getAllTransactions(userid, page, offset) {
    try {
      const templateurl = `${endpoint}gain/transactions?userId=${userid}&page=${page}&offset=${offset}`;
      const resp: any = await this.http.get(templateurl,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  parseJwt(token: string) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('')
      .map(c => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)).join(''));

    return JSON.parse(jsonPayload);
  }

  async getChallenges(userid: number) {

    try {
      const resp: any = await this.http.get(`${endpoint}challenge/getactivechallenges?userId=${userid}`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async getChallengesPaged(userid, page, offset) {
    try {
      const templateurl = `${endpoint}challenge/getactivechallengespaged?userId=${userid}&page=${page}&offset=${offset}`;
      const resp: any = await this.http.get(templateurl,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async getFinishedChallenges(userid: number) {
    try {
      const resp: any = await this.http.get(`${endpoint}challenge/getfinishedchallenges?userId=${userid}`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async getFinishedChallengesPaged(userid, page, offset) {
    try {
      const templateurl = `${endpoint}challenge/getfinishedchallengespaged?userId=${userid}&page=${page}&offset=${offset}`;
      const resp: any = await this.http.get(templateurl,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async getChallengeById(userid: number, challengeid: number) {
    try {
      const resp: any = await this.http.get(`${endpoint}challenge/getchallengebyid?userId=${userid}&challengeId=${challengeid}`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async getResultsByChallenge(challengeid: number) {
    try {
      const resp: any = await this.http.get(`${endpoint}challenge/getresultsbychallenge?challengeId=${challengeid}`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async enterChallenge(contender: any) {

    try {
      return this.http.post(`${endpoint}challenge/newcontender`, contender, {
        headers: {
          Authorization: 'Bearer ' + await this.getAccessToken(),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async getUserById() {

    try {
      return this.http.get(`${endpoint}user/getuser`, {
        headers: {
          Authorization: 'Bearer ' + await this.getAccessToken(),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async getUserByEmail(user: any) {

    try {
      return this.http.post(`${endpoint}user/getbyemail`, user, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async getUserByUUID(uuid: any) {
    const user = {} as UserLogin;
    user.uuid = uuid;
    try {
      return this.http.post(`${endpoint}user/getbyuuid`, user, {
        headers: {
          Authorization: 'Bearer ' + await this.getAccessToken(),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async confirmOrCancelGuest(uuid: string, userid: number, confirmorcancel: number) {
    const postData: any = { uuid, userid, confirmorcancel };
    try {
      return this.http.post(`${endpoint}account/confirmorcancelguest`, postData, {
        headers: {
          Authorization: 'Bearer ' + await this.getAccessToken(),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async earnPoints(points: number, type = 4) {
    const gain: Gain = { userId: await this.getUserId(), matchId: 0, type, challengeId: 0, gainedPoints: points };
    try {
      const resp: any = this.http.post(`${endpoint}gain/new`, gain, {
        headers: {
          Authorization: 'Bearer ' + await this.getAccessToken(),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
      return resp;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async createTransaction(data: { transactionId: string; receipt: string; signature: string; productType: string; }) {
    try {
      return this.http.post(`${endpoint}transaction/new`, data, {
        headers: {
          Authorization: 'Bearer ' + await this.getAccessToken(),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async createActionLog(data: { userid: number; action: string; }) {
    try {
      return this.http.post(`${endpoint}action/new`, data, {
        headers: {
          Authorization: 'Bearer ' + await this.getAccessToken(),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async getRankings(year: number, month: number) {
    try {
      const resp: any = await this.http.get(`${endpoint}match/getrankings?year=${year}&month=${month}`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async updateUserName(userName: string) {
    const data = { userName };
    try {
      return this.http.post(`${endpoint}account/updateusername`, data, {
        headers: {
          Authorization: 'Bearer ' + await this.getAccessToken(),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async updateSubscriptionId(subscriptionId: string) {
    const data = { subscriptionId };
    try {
      return this.http.post(`${endpoint}account/updatesubscriptionid`, data, {
        headers: {
          Authorization: 'Bearer ' + await this.getAccessToken(),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async readNotification(notificationId: number) {
    const data = { userId: await this.getUserId(), notificationId };
    try {
      return this.http.post(`${endpoint}notification/read`, data, {
        headers: {
          Authorization: 'Bearer ' + await this.getAccessToken(),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async getNotifications() {
    try {
      const resp: any = await this.http.get(`${endpoint}notification/list`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async getBuyables() {

    try {
      const resp: any = await this.http.get(`${endpoint}prize/buyables`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async getBuyOrders() {

    try {
      const resp: any = await this.http.get(`${endpoint}prize/buyorders`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async createBuyOrder(buyorder: any) {

    try {
      return this.http.post(`${endpoint}prize/buyorder`, buyorder, {
        headers: {
          Authorization: 'Bearer ' + await this.getAccessToken(),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).toPromise();
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }
  }

  async getCoin() {

    try {
      const resp: any = await this.http.get(`${endpoint}prize/coin`,
        {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();

      return resp.data;
    } catch (e) {
      if (e.status === 403) {
        this.logout();
      }
    }

  }

  async sendReference(referenceUserId: number) {
      const data = { referenceUserId };
      try {
        return this.http.post(`${endpoint}account/updatereferenceid`, data, {
          headers: {
            Authorization: 'Bearer ' + await this.getAccessToken(),
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }).toPromise();
      } catch (e) {
        if (e.status === 403) {
          this.logout();
        }
      }
  }



  logout() {
    this.storage.remove('user');
    this.router.navigate(['']);
  }



}
