import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { Storage } from '@ionic/storage';
import { AlertController, LoadingController } from '@ionic/angular';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SharedService {
  isLoading = false;
  public loggedin = new Subject<any>();
  loggedin$ = this.loggedin.asObservable();

  constructor(private rest: RestService,
    private storage: Storage,
    public loadingController: LoadingController,
    private alertController: AlertController) {

  }

  getMatchDate(num) {
    const day = num.toString().substring(0, 2);
    const month = num.toString().substring(2, 4);
    const year = num.toString().substring(4, 8);
    const hour = num.toString().substring(8, 10);
    const minute = num.toString().substring(10, 12);
    return new Date(year + '-' + month + '-' + day + ' ' + hour + ':' + minute);
  }

  dhms(t) {
    const days = Math.floor(t / 86400);
    t -= days * 86400;
    const hours = Math.floor(t / 3600) % 24;
    t -= hours * 3600;
    const minutes = Math.floor(t / 60) % 60;
    t -= minutes * 60;
    const seconds = t % 60;

    return [
      days + 'd',
      hours + 'h',
      minutes + 'm',
      seconds + 's'
    ].join(' ');
  }

  async getPoints() {

    let userid = await this.rest.getUserId();
    const user: any = await this.rest.getUserById();
    PointsClass.points = user.data.points;
    // const gains: any[] = await this.rest.getPoints(userid);
    // const bets: any[] = await this.rest.getPreviousBetsByUser(userid);
    // const challenges: any[] = await this.rest.getChallenges(userid);

    // const totalgains = gains.map(x => x.gainedPoints).reduce((ty, u) => ty + u, 0);
    // const totalbets = bets.filter(x => x.DeletedAt === null).map(x => x.points).reduce((ty, u) => ty + u, 0);
    // const totalchallenges = challenges.
    //   filter(x => x.Contenders.some(y => y.userId === userid)).
    //   map(x => x.minPoints).reduce((ty, u) => ty + u, 0);

    // PointsClass.points = totalgains - totalbets - totalchallenges;
    return user.data.points;

  }

  async getCoins() {

    const coins = await this.rest.getCoin();
    PointsClass.coins = parseFloat(coins);
  }

  async setStorage(key, value) {
    await this.storage.set(key, JSON.stringify(value));
  }

  async getStorage(key) {
    const val = await this.storage.get(key);
    return JSON.parse(val);
  }

  async removeKey(key) {
    await this.storage.remove(key);
  }

  async presentLoading() {
    this.isLoading = true;
    return await this.loadingController.create({
      duration: 5000,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismissLoading() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentAlert(header, message) {
    const alert = await this.alertController.create({
      header,
      message,
      buttons: ['OK']
    });
    await alert.present();
  }

}
@Injectable({
  providedIn: 'root',
})
export class PointsClass {
  public static points = 0;
  public static coins = 0;
}


@Injectable({
  providedIn: 'root',
})
export class ActiveAd {
  public static adid = '';
}