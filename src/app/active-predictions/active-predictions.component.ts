import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { RestService } from '../services/rest.service';
import { ModalController } from '@ionic/angular';
import { SharedService } from '../services/shared.service';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';


@Component({
  selector: 'app-active-predictions',
  templateUrl: './active-predictions.component.html',
  styleUrls: ['./active-predictions.component.scss'],
})
export class ActivePredictionsComponent implements OnInit {
  @Input() isactive = false;
  bets: any = [];

  constructor(private readonly rest: RestService,
              private readonly modalController: ModalController,
              private readonly sharedService: SharedService,
              private readonly translate: TranslateService,
              private readonly ref: ChangeDetectorRef,
              private readonly globalization: Globalization) { }

  async ngOnInit(): Promise<void> {
    this.sharedService.presentLoading();
    if (this.isactive) {
      this.bets = await this.rest.getActiveBets();
    } else {
      this.bets = await this.rest.getFinishedBets();
    }
    this.sharedService.dismissLoading();
    this.ref.detectChanges();
  }

  dismiss() {
    this.modalController.dismiss();
  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }

}
