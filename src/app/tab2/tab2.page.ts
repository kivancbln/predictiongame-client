import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { RestService } from '../services/rest.service';
import { LoginResponse } from '../interfaces/login-response';
import { scan } from 'rxjs/operators';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  user: LoginResponse;
  transactions: any[] = [{ CreatedAt: '03/03/2020' }];
  transactions$: any;
  newTransactions: any;
  page: number;
  offset: number;


  constructor(private readonly rest: RestService,
              private readonly ref: ChangeDetectorRef,
              private readonly sharedService: SharedService,
              private readonly globalization: Globalization,
              public translate: TranslateService) {
    this.page = 1;
    this.offset = 10;
  }
  async ngOnInit(): Promise<void> {
    this.sharedService.presentLoading();
    const userid  = await this.rest.getUserId();
    this.newTransactions = await this.rest.getAllTransactions(userid, this.page, this.offset);


    this.transactions$ = this.rest.transactionChanges$
      .pipe(
        scan((acc, tr: any) => {
          return {
            ...acc,
            ...tr,
          };
        }, this.transactions),
      );

    this.transactions$.subscribe(res => {
      this.transactions = Object.values(res);
    });

    this.transactions = this.rest.transactions;

    this.ref.detectChanges();
    this.sharedService.dismissLoading();

  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }

  identify(item) {
    return item.CreatedAt;
  }

  doInfinite(event): Promise<any> {


    return new Promise((resolve) => {
      setTimeout(async () => {
        this.page = this.page + 1;
        const userid  = await this.rest.getUserId();
        const trs = await this.rest.getAllTransactions(userid, this.page, this.offset);
        if (trs.length > 0) {
          for (const tr of trs) {
            this.newTransactions.push(tr);
          }
          event.target.complete();
        } else {
          event.target.disabled = true;
        }


        resolve();
      }, 1000);
    });
  }

  doRefresh(event): Promise<any> {


    return new Promise((resolve) => {
      setTimeout(async () => {
        this.page = 1;
        const userid  = await this.rest.getUserId();
        const trs = await this.rest.getAllTransactions(userid, this.page, this.offset);

        this.newTransactions = trs;
        event.target.complete();



        resolve();
      }, 1000);
    });
  }

}
