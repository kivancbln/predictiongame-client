import { Component, OnInit, ChangeDetectorRef, HostListener } from '@angular/core';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig, AdMobFreeRewardVideoConfig } from '@ionic-native/admob-free/ngx';
import { LoadingController, ToastController } from '@ionic/angular';
import { RestService } from '../services/rest.service';
import { SharedService, ActiveAd } from '../services/shared.service';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {
  isLoading = false;
  user: any;
  balls: number[];


  constructor(private admobFree: AdMobFree,
    public loadingController: LoadingController,
    private rest: RestService,
    private sharedService: SharedService,
    private ref: ChangeDetectorRef,
    private globalization: Globalization,
    private translate: TranslateService,
    private toastController: ToastController) { }

  async ngOnInit(): Promise<void> {
    /*document.addEventListener('admob.rewardvideo.events.REWARD', async (result: any) => {
      console.log(result);
      if (result.returnValue) {
        await this.pointsEarned(result.rewardAmount);
      }

    });

    document.addEventListener('admob.rewardvideo.events.CLOSE', (result) => {
      alert('video closed');
      this.dismiss();
    });

    document.addEventListener('admob.rewardvideo.events.LOAD_FAIL', (result) => {
      alert('video not available');
      this.dismiss();
    });

    document.addEventListener('admob.rewardvideo.events.EXIT_APP', (result) => {
      this.dismiss();
    });*/

    //this.balls = Array<number>(7);

    this.user = this.rest.loggedInUser['account'];
    this.balls = Array<number>(parseInt(this.user.actualComboCount, 10));

  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }


  async pointsEarned(rewardAmount: any) {
    this.presentLoading();
    const gain = await this.rest.earnPoints(rewardAmount);
    console.log('gain', gain);
    this.sharedService.getPoints();
    this.ref.detectChanges();
    this.dismiss();
    this.presentToast(this.translate.instant('Points loaded: ') + gain.gain.gainedPoints);


  }

  async showRewardVideoAds() {
    try {
      ActiveAd.adid = 'combo';
      const RewardVideoConfig: AdMobFreeRewardVideoConfig = {
        // isTesting: true, // Remove in production
        autoShow: true, // ,
        id: 'ca-app-pub-3709541018243663/5816535635'
      };
      this.admobFree.rewardVideo.config(RewardVideoConfig);
      this.presentLoading();
      const res = await this.admobFree.rewardVideo.prepare();

    } catch (e) {
      alert(e);
    }
  }

  async presentLoading() {
    this.isLoading = true;
    return await this.loadingController.create({
      duration: 5000,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  getGainedPoints(idx) {
    return Math.pow(2, idx + 1);
  }

  getWatchedString() {

    const lwd = new Date(this.user.lastWatchDate);
    const now = new Date();
    const newlwd = new Date(lwd.getFullYear(), lwd.getMonth(), parseInt(lwd.toISOString().split('T')[0].split('-')[2], 10));
    const calculatedDay = (now.getTime() - newlwd.getTime()) / (1000 * 3600 * 24);
    if (calculatedDay > 1 && calculatedDay < 2) {
      return this.translate.instant('Watch now to keep daily bonus');
    } else if (calculatedDay < 1) {
      return '';
    } else {
      return this.translate.instant('Watch now to begin earning daily bonus');
    }
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 5000
    });
    toast.present();
  }

  @HostListener('document:admob.rewardvideo.events.REWARD', ['$event'])
  async rewardEnter(event: any) {
    if (ActiveAd.adid === 'combo' && event.returnValue) {
      await this.pointsEarned(event.rewardAmount);
      ActiveAd.adid = '';
    }
  }

  @HostListener('document:admob.rewardvideo.events.CLOSE', ['$event'])
  async closeEnter(event: any) {
    if (ActiveAd.adid && !event.returnValue) {
      alert('video closed');
      this.dismiss();
      ActiveAd.adid = '';
    }
  }

  @HostListener('document:admob.rewardvideo.events.LOAD_FAIL', ['$event'])
  async loadFailEnter(event: any) {
    if (ActiveAd.adid) {
      alert('video load failed');
      this.dismiss();
      ActiveAd.adid = '';
    }
  }

  @HostListener('document:admob.rewardvideo.events.EXIT_APP', ['$event'])
  async exitAppEnter(event: any) {
    this.dismiss();
  }

}
