import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { RestService } from '../services/rest.service';
import { NavParams, AlertController, ToastController } from '@ionic/angular';
import { SharedService } from '../services/shared.service';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-challenge-bet',
  templateUrl: './challenge-bet.component.html',
  styleUrls: ['./challenge-bet.component.scss'],
})
export class ChallengeBetComponent implements OnInit {
  modalCtrl: any;
  challenge: any;
  userid: number;
  ismatches = true;
  results: any;
  allPoints: number;
  matchPoints: {};

  constructor(
    private readonly rest: RestService, navParams: NavParams,
    public alertController: AlertController,
    private readonly ref: ChangeDetectorRef,
    private readonly toastController: ToastController,
    private readonly shared: SharedService,
    private readonly globalization: Globalization,
    private readonly translate: TranslateService
  ) {

    this.modalCtrl = navParams.get('modalCtrl');
    this.challenge = navParams.get('challenge');
    this.allPoints = 0;
    for (const match of this.challenge.Matches) {
      for (const bet of match.Match.Bets) {
        this.allPoints = this.allPoints + bet.points;
      }
    }

  }

  async ngOnInit(): Promise<void> {
    this.userid = await this.rest.getUserId();
    this.results = await this.rest.getResultsByChallenge(this.challenge.ID);
    const usr: any = this.rest.loggedInUser;
    const userid = usr === undefined ? -1 : usr.account.ID;
    // this.rest.createActionLog({ userid, action: 'Challenge bet page entered' });
  }

  ionViewDidEnter() {
    this.ref.detectChanges();
    this.globalization.getPreferredLanguage().then(lg => {
      if (lg.value === 'tr-TR') {
        this.translate.use(lg.value);
      }
    });
  }

  dismiss(flag) {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      challengeid: this.challenge.ID,
      dismissed: flag
    });
  }

  async enterChallenge() {

    this.shared.presentLoading();
    const usr: any = this.rest.loggedInUser;
    const guest = usr.account.guest;
    if (guest === true) {
      this.presentAlert(this.translate.instant('You can not enter challenge as guest user.'), this.translate.instant('Info'));
    } else {
      if (this.challenge.Matches.some(x => (new Date(x.Match.date)) < (new Date()))) {
        this.presentAlert(this.translate.instant('You can not enter challenge after matches started.'), this.translate.instant('Info'));
      } else {
        const contender: any = {};
        contender.challengeId = this.challenge.ID;
        contender.userId = this.userid;
        const points = await this.shared.getPoints();
        const chpoints = this.challenge.minPoints;
        if (points >= chpoints) {
          const ret: any = await this.rest.enterChallenge(contender);
          if (ret.status) {
            this.challenge = (await this.rest.getChallengeById(this.userid, this.challenge.ID))[0];
            this.shared.dismissLoading();
            this.presentAlert(
              this.translate.instant('You entered this challenge, if you don\'t complete the challenge you will get refund.') + '\n' +
              this.translate.instant('To complete you need to make predictions to all matches in challenge.'),
              this.translate.instant('Info'));
          } else {
            this.shared.dismissLoading();
            this.presentAlert(this.translate.instant('Some error has occured.'));
          }
        } else {
          this.shared.dismissLoading();
          this.presentAlert(this.translate.instant('Insufficient points.'));
        }
      }
    }
    this.shared.dismissLoading();
  }

  async presentAlert(message, header = 'Denied') {
    const alert = await this.alertController.create({
      header,
      message,
      buttons: ['OK']
    });
    await alert.present();
  }

  segmentChanged(e) {
    this.ismatches = e.detail.value === 'matches';
  }

  async pointsChange(e) {
    this.challenge.Matches.find(x => x.Match.ID === e.ID).Match.Bets = e.Bets;
    this.allPoints = 0;
    for (const match of this.challenge.Matches) {
      for (const bet of match.Match.Bets) {
        this.allPoints = this.allPoints + bet.points;
      }
    }
    console.log(this.allPoints);
  }


}
