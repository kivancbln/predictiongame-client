import { Component, OnInit } from '@angular/core';
import { ModalController, MenuController, NavController } from '@ionic/angular';
import { RegisterComponent } from '../auth/register/register.component';
import { LoginComponent } from '../auth/login/login.component';
import { RestService } from '../services/rest.service';
import { Router } from '@angular/router';
import { SocketService } from '../services/socket.service';
import { VERSION } from '../../environments/version';
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingPage implements OnInit {
  version = VERSION;
  constructor(
    private modalController: ModalController,
    private menu: MenuController,
    private rest: RestService,
    private navCtrl: NavController,
    private router: Router,
    private socket: SocketService
  ) {
    this.menu.enable(false);
  }
  async ionViewWillEnter() {
    const token: string = await this.rest.getAccessToken();

    if (token) {
      const expiresAt: any = this.rest.parseJwt(token);
      if (Date.now() / 1000 < parseInt(expiresAt.exp, 10)) {
        this.router.navigateByUrl('/dashboard/tabs/tab1');
      }

    }

  }

  async ionViewDidEnter(): Promise<void> {
    await this.login();
  }

  async ngOnInit(): Promise<void> {
    // await this.login();
  }
  async register() {
    const registerModal = await this.modalController.create({
      component: RegisterComponent
    });
    return await registerModal.present();
  }
  async login() {
    
    const loginModal = await this.modalController.create({
      component: LoginComponent,
      componentProps: {},
      showBackdrop: true,
      backdropDismiss: false,
    });
    return await loginModal.present();
  }



}
