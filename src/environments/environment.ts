// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  config : {
    apiKey: "AIzaSyBSVTaBjwDPu21Wv-YTFKbpI4T5-9Ok0m0",
    authDomain: "fleet-lightning-200911.firebaseapp.com",
    databaseURL: "https://fleet-lightning-200911.firebaseio.com",
    projectId: "fleet-lightning-200911",
    storageBucket: "fleet-lightning-200911.appspot.com",
    messagingSenderId: "533856061812",
    //appId: "1:533856061812:web:4c096364af739d879e0421",
    appId: "1:533856061812:android:4d3a15a0be6ef5529e0421"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
